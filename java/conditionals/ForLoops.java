package java.conditionals;


public class ForLoops {
    public static void main(String[] args) {
        
        for(int i = 0; i <= 1000; i++) {
            System.out.println("int i = " + i);
            
        }
        // This code continuously increments integer i by 1 until it is equal to 1000. Then it prints the integer into the console.
        
        //This is an example of a for-each loop 
        // This is a narray and it is used to store multiple integers insside a single variable.
        int[] intArray = {0,1,2,3,4,5};

        for (int p: intArray) {
            System.out.println(p);
        }
        
        /* For each integer in the array called intArray print out it's value. */ 
    }
}