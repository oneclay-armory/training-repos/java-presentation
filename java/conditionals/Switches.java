package java.conditionals;
import java.util.Scanner;


public class Switches {
    public static void main(String[] args) {
        // Creates scanner object 
        Scanner scanObj = new Scanner(System.in);
        System.out.println("Input a number between 1 and 7");
        // Asks user to input a day. 
        int day = scanObj.nextInt();
        // Switch statement takes the input and sees if it  qualifies for the predetermined cases. If it does it will run the code under the correct case. 
        switch(day) {

            case 1:
                System.out.println("The day is Monday");

                break;      
            case 2:
                System.out.println("The day is tuesday");
                break;
            case 3:
                System.out.println("The day is wednesday");
            
                break;

            case 4:
                System.out.println("The day is thursday");
                break;

            case 5:
                System.out.println(" The day is Friday");
                break;

            case 6:
                System.out.println("The day is Saturday");
                break;

            case 7:
                System.out.println("The day is Sunday");
                break;
            // This default statement runs if none of the cases are correct. 
            default:
                System.out.println("Thee are only 7 days in a week!");
        }
    }
}
