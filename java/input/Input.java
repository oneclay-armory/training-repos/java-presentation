package java.input; // Specifies file location 
// Specifies that we want to import classes from java.util 
import java.util.*;


public class Input {    
    public static void main (String[] arsg ) {
        //The code below defines a scanner object called input.
        Scanner input = new Scanner(System.in);
        //This asks the user what their name is.
        System.out.println("what is your name?");
        // This prints out their name after they inputted it. 
        System.out.println("Your name is :" + input.nextLine());
        // Closes the scanner object 
        input.close();

    }
   
    
}
