package java.methods;

import java.util.Scanner;

// Both examples do the same thing. 
public class MethodEx {
    // This method does not have to be static. 
    static void add (int x , int y) {
        System.out.println(x +y);
    }
    public static void main(String[] args) {
    
        add(4,3);
    }
    
}

// public class MethodEx {
    
//     public void add (int x , int y) {
//         System.out.println(x +y);
//     }
//     public static void main(String[] args) {
//         MethodEx met = new MethodEx(); 
//         met.add(4,3);
//     }
    
// }