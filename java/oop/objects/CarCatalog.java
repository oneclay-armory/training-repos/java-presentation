package java.oop.objects;


public class CarCatalog {

    // This example serves as a catalog that prints data about each car object.
    // The previous file served as a template for informatino that would be displayed about a car. 
    // Then, this information is passed through methods that print out data about it.
    
    
    public static void main(String[] args ) {
        // These two lines of code are creating new instances of the car object with parameters that are defined in the constructor. 
        Car Audi = new Car(100, 50, 10);
        Car Truck = new Car(80, 100, 8);
        //These three lines use the "Audi" object to print out data that is associated with it 
        Audi.printAccel();
        Audi.printSpeed();
        Audi.printTorque();
        // Blank line
        System.out.println(" ");
        // Invokes the printAlldata method. 
        Truck.printAllData();


    }
    
}